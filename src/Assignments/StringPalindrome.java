package Assignments;
import java.util.Scanner;

public class StringPalindrome {

	public static void main(String[] args) {
		String string, rev="";
		System.out.println("Enter a string");
		Scanner sc= new Scanner(System.in);
		string=sc.nextLine();
		int length = string.length();
		for(int i= length-1;i>=0;i--) {
			rev = rev +string.charAt(i);
		}
		if(string.equalsIgnoreCase(rev)) {
			System.out.println(string+"is a palindrome!");
		}else
			System.out.println(string + "is not a palindrome!");

	}

}
